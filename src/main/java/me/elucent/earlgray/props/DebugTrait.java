package me.elucent.earlgray.props;

import me.elucent.earlgray.api.Trait;
import me.elucent.earlgray.api.Traits;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.nbt.CompoundTag;

public class DebugTrait extends Trait {
    int val = 5;

    @Override
    public CompoundTag write(CompoundTag tag) {
        tag.putInt("val", val);
        return tag;
    }

    @Override
    public Trait read(CompoundTag tag) {
        val = tag.getInt("val");
        return this;
    }

    @Override
    public void update(Entity e) {
        if (e.age % 40 == 0) {
            val --;
            e.damage(DamageSource.CACTUS, 1);
            markDirty();
            if (val <= 0) {
                Traits.remove(e, this);
            }
        }
    }

    @Override
    public boolean isTickable() {
        return true;
    }
}
