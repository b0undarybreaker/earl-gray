package me.elucent.earlgray.api;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class TraitRegistry {
    static Map<Identifier, TraitEntry> registry = new HashMap<>();
    static Map<Class<? extends Trait>, TraitEntry> classmap = new HashMap<>();
    static Map<Class<? extends Entity>, List<Function<Entity, Trait>>> inherents = new HashMap<>();
    static Map<Class, List<Class>> hierarchies = new HashMap<>();

    public static TraitEntry<? extends Trait> register(Identifier id, Class<? extends Trait> traitClass) {
        return register(new TraitEntry<>(id, traitClass));
    }

    public static TraitEntry<? extends Trait> register(TraitEntry<? extends Trait> entry) {
        if (registry.containsKey(entry.getName())) {
            System.out.println("Unable to register trait with identifier "
                    + entry.getName() + ", said identifier is already registered!");
            return null;
        }
        registry.put(entry.getName(), entry);
        classmap.put(entry.traitClass, entry);
        return entry;
    }

    public static <T extends Trait> T generate(TraitEntry<T> entry, CompoundTag tag) {
        return (T)registry.get(entry.getName()).generate(tag);
    }

    public static <T extends Trait> T generate(Identifier id, CompoundTag tag) {
        return (T)registry.get(id).generate(tag);
    }

    public static TraitEntry getEntry(Identifier id) {
        return registry.get(id);
    }

    public static <T extends Trait> TraitEntry<T> getEntry(T trait) {
        return (TraitEntry<T>)getClassEntry(trait.getClass());
    }

    public static <T extends Trait> TraitEntry<T> getClassEntry(Class<T> c) {
        TraitEntry<T> e = classmap.get(c);
        if (e != null) return e;
        else return null;
    }

    public static <T extends Entity> void addInherent(Class<T> entityClass, Function<T, Trait> trait) {
        List<Function<Entity, Trait>> traits = inherents.get(entityClass);
        if (traits == null) {
            inherents.put(entityClass, traits = new ArrayList<>());
        }
        traits.add((Function<Entity, Trait>)trait);
    }

    public static void applyInherents(Entity entity) {
        List<Class> classes = hierarchies.get(entity.getClass());
        if (classes == null) {
            classes = new ArrayList<>();
            Class c = entity.getClass();
            while (c != null) {
                classes.add(c);
                c = c.getSuperclass();
            }
            hierarchies.put(c, classes);
        }
        for (Class c : classes) {
            List<Function<Entity, Trait>> l = inherents.get(c);
            if (l != null) {
                for (Function<Entity, Trait> p : l) ((TraitHolder)entity).getTraits().addTrait(p.apply(entity));
            }
        }
    }
}
